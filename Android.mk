include $(call all-subdir-makefiles)

include $(CLEAR_VARS)

TA_LIBS := libpowerstatshaldataprovider.so
TA_SYMLINKS := $(addprefix $(TARGET_OUT_SYSTEM_EXT_APPS_PRIVILEGED)/TurboAdapter/lib/arm64/,$(notdir $(TA_LIBS)))
$(TA_SYMLINKS): $(LOCAL_INSTALLED_MODULE)
	@echo "TurboAdapter lib link: $@"
	@mkdir -p $(dir $@)
	@rm -rf $@
	$(hide) ln -sf /system_ext/lib64/$(notdir $@) $@

ALL_DEFAULT_INSTALLED_MODULES += $(TA_SYMLINKS)
